For DB:

https://cloud.mongodb.com/v2/60b3311d1ad96b0e40022b1c#clusters/detail/mini-cupcake-2

## To Start For the First Time

1. Rename the ".env.local" to ".env", the .env file

- It contain the PORT for backend and MONGO_URI to connect to DB in mongoDB Atlas

2. Run in terminal

"npm install" in root and in frontend folder

- to install all the dependencies

3. In root terminal

"npm run dev"

- It will automatically run both frontend and backend

## If want to change any data

Change it in here
https://cloud.mongodb.com/v2/60b3311d1ad96b0e40022b1c#clusters/detail/mini-cupcake-2
